import * as firebase from 'firebase/app';

let gApp: firebase.app.App;

export function setApp(app: firebase.app.App) {
    gApp = app;
}

export function getApp(): firebase.app.App {
    return gApp;
}
