import * as _ from 'lodash';
import * as redux from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import invariant from 'redux-immutable-state-invariant';

import 'firebase/auth';
import 'firebase/database';

import { Observable, Subject, Subscriber } from 'rxjs';

import { getApp } from './app';

import { FirebaseRTDB as FirebaseRTDBResourceHnadler } from './resource-handlers/firebase-rtdb';
import { Local as LocalResourceHnadler } from './resource-handlers/local';

import { Walker, EventLike, ResourceQuery, ThunkLike } from './backend-interfaces';

import storeReducer from './store-reducer';

import { Action } from './actions/action';
import { userSignedIn } from './actions/user-signed-in';
import { ResourceHandler } from './resource-handlers/resource-handler';

function distinctInsert<T>(arr: T[], what: T): T[] {
    const idx = _.indexOf(arr, what);
    if (idx !== -1) {
        return arr;
    } else {
        return _.concat(arr, what);
    }
}

export const resourceHandlers: { [key: string]: ResourceHandler } = {
    'local': new LocalResourceHnadler(),
    'rtdb': new FirebaseRTDBResourceHnadler(),
};

class WalkerImpl implements Walker {
    getters: any[] = [];
    constructor(
        private backend: Backend,
        private sub: Subscriber<Walker>,
    ) { }
    get<T>(q: ResourceQuery<T> | string): T {
        const qry = typeof q === 'string' ? { type: 'local', path: q } as ResourceQuery<T> : q;
        const storeKey = resourceHandlers[qry.type].toString(qry);

        this.getters = distinctInsert(this.getters, storeKey);
        return this.backend.get(q);
    }
    getAsync<T>(q: ResourceQuery<T> | string): Promise<T> {
        const qry = typeof q === 'string' ? { type: 'local', path: q } as ResourceQuery<T> : q;
        const storeKey = resourceHandlers[qry.type].toString(qry);

        this.getters = distinctInsert(this.getters, storeKey);
        return this.backend.getAsync(q);
    }
    newKey(q: ResourceQuery<string>): Promise<string | null> {
        return this.backend.newKey(q);
    }
    dispatch<T>(event: EventLike | ThunkLike<T>): Promise<any> {
        return this.backend.dispatch(event);
    }
    next(val: string | null) {
        if (val == null || _.includes(this.getters, val)) {
            this.getters = [];
            this.sub.next(this);
        }
    }
}

export class Backend implements Walker {
    redux: any;
    private sbj: Subject<string> = new Subject();
    snapshoters: {
        [key: string]: ((event: Action, w: Walker) => any),
    } = {};
    streamSnapshoter?: ((event: Action, w: Walker) => any);
    streamOnlyExternal = true;

    constructor(
    ) {
        this.redux = redux.createStore(storeReducer, composeWithDevTools(
            redux.applyMiddleware(invariant()),
        ));

        const app = getApp();
        if (app) {
            app.auth().onAuthStateChanged((listener) => {
                this.dispatch(userSignedIn(listener ? listener.uid : undefined, listener && listener.email ? listener.email : undefined));
            });
        }
    }
    get<T>(q: ResourceQuery<T> | string): T {
        const qry = typeof q === 'string' ? { type: 'local', path: q } as ResourceQuery<T> : q;
        const storeKey = resourceHandlers[qry.type].toString(qry);

        const val = _.get(this.redux.getState(), [storeKey]);
        if (val !== undefined) {
            return val;
        }
        resourceHandlers[qry.type].ensureLoaded(qry, this);
        return null as any;
    }
    async getAsync<T>(q: ResourceQuery<T> | string): Promise<T> {
        const qry = typeof q === 'string' ? { type: 'local', path: q } as ResourceQuery<T> : q;
        const storeKey = resourceHandlers[qry.type].toString(qry);

        const val = _.get(this.redux.getState(), [storeKey]);
        if (val !== undefined) {
            return Promise.resolve(val);
        }
        return resourceHandlers[qry.type].ensureLoaded(qry, this);
    }
    newKey(q: ResourceQuery<string>): Promise<string | null> {
        return resourceHandlers[q.type].newKey(q);
    }

    async dispatch<T>(action: EventLike | ThunkLike<T>): Promise<T> {
        let event: any;
        // TODO - improve typing, because event can be a lot of things during runtime, and it's confusing even for me to understand

        if (_.isFunction(action)) {
            event = (action as ThunkLike<T>)(this);
            if (event instanceof Promise) {
                event = await event;
            }
        } else {
            event = action;
        }
        if (event == null) {
            return event;
        }
        // get global snapshoters and add local one
        const snapshoters = _.merge({}, this.snapshoters);
        const embedSnapshoterName = '@';
        snapshoters[embedSnapshoterName] = _.get(event, 'snapshoter');
        // get list of snapshoter names
        const snapshoterNames = _.keys(snapshoters);
        const streamSnapshoterName = '@@';
        if (this.streamSnapshoter) {
            // stream snapshoter needs to be at the end
            snapshoterNames.push(streamSnapshoterName); // added to the end
            snapshoters[streamSnapshoterName] = this.streamSnapshoter; // made availble in the object
        }

        const batches: { [key: string]: any } = {};
        // ask snapshoters for their data
        for (let idx = 0; idx < snapshoterNames.length; ++idx) {
            const snapname = snapshoterNames[idx];
            const snapshoter = snapshoters[snapname];
            let res = null;
            if (snapshoter == null) {
                if (snapname !== embedSnapshoterName) {
                    continue;
                }
                res = _.get(event, 'snapshot');
            } else {
                if (snapname === streamSnapshoterName) {
                    const batchNames = _.keys(batches);
                    if (this.streamOnlyExternal && (batchNames.length === 0 || _.isEqual(batchNames, ['local']))) {
                        continue;
                    }
                }
                res = snapshoter(event, this);
                if (res instanceof Promise) {
                    res = await res;
                }
                if (res != null) {
                    _.set(event, ['snapshots', snapname], res);
                }
            }
            if (res == null) {
                continue;
            }
            _.forIn(res, (dataVal, dataKey) => {
                const pieces = dataKey.split(':');
                let resultKey = dataKey;
                if (pieces[1] !== 'local') {
                    resultKey = _.slice(pieces, 2).join(':');
                }
                _.set(batches, [pieces[1], resultKey], dataVal);
            });
        }
        // add cache to batches
        _.forIn(event['cache'], (dataVal, dataKey) => {
            _.set(batches, ['local', dataKey], dataVal);
        });

        // send the full event to redux, so locals will be ready for notifications
        const result = this.redux.dispatch(event);

        // send to resouce providerds, except local which is already updated so distribute notifications
        const batchResourceHandlers = _.keys(batches);
        for (let idx = 0; idx < batchResourceHandlers.length; ++idx) {
            const key = batchResourceHandlers[idx];
            const val = batches[key];
            if (key === 'local') {
                // notify all state observables
                _.forIn(val, (lval, lkey) => {
                    const lkeyParts = lkey.split('.');
                    this.sbj.next(lkeyParts[0]);
                });
            } else {
                await resourceHandlers[key].set(val);
            }
        }
        // return what dispatch returned
        return result;
    }

    state(): Observable<Walker> {
        return new Observable((sub) => {
            const dbWrap = new WalkerImpl(this, sub);
            dbWrap.next(null);
            const subs = this.sbj.subscribe((val) => {
                dbWrap.next(val);
            });
            return function () {
                subs.unsubscribe();
                sub.complete();
            };
        });
    }
}
