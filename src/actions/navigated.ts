import { Action } from './action';

export const NavigatedActionType = '@navigated';

export interface NavidatedActionPayload {
    url: string;
    params: any;
    query: any;
}

export function navigated(data: NavidatedActionPayload): Action {
    return {
        type: NavigatedActionType + ' - ' + data.url,
        payload: data,
    };
}
