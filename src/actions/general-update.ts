import * as _ from 'lodash';

import { Walker, SnapshotGenerator, ResourceQuery, loggedUserUrn } from '..';

export function generalUpdateAction(urn: ResourceQuery<any>, deltas: {[key: string]: any}, title = 'anonymous') {
    return async (w: Walker) => {
        const sn = new SnapshotGenerator();
        sn.setBatch(urn, deltas);
        return {
            type: 'GU: ' + title,
            payload: {
                path: SnapshotGenerator.resourceQueryToString(urn),
                deltas,
            },
            snapshot: sn.snapshot
        };
    };
}
