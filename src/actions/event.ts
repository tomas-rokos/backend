import { now } from 'moment';

export const EventActionType = '@event';

export function event(name: string, value: any = now(), customType = null) {
    return {
        type: customType ? customType : EventActionType + ' - ' + name,
        payload: { name, value },
        cache: {['urn:local:' + name]: value},
    };
}
