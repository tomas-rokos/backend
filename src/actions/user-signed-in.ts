import { Action } from './action';

export const UserSignedInActionType = '@user-signed-in';

export interface UserSignedInActionPayload {
    uid: string;
    email: string;
}

export function userSignedIn(uid?: string, email?: string): Action {
    return uid != null && email != null ?
        { type: UserSignedInActionType + ' - ' + email, payload : { uid, email } }
        :
        { type: UserSignedInActionType + ' - NO' };
}
