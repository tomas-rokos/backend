import { SnapshotGenerator} from './snapshot-generator';
import { ResourceQuery, FirebaseRTDBQuery } from './backend-interfaces';

describe('snapshot-generator', async () => {
    it('just nothing', () => {
        const sn = new SnapshotGenerator();
        expect(sn.snapshot).toEqual({});
    });
    it('set: local urn', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'local', path: 'abcd'}, 'text');
        expect(sn.snapshot).toEqual({'urn:local:abcd': 'text'});
    });
    it('set: local urn with subpath', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'local', path: 'abcd'}, true, 'ef');
        expect(sn.snapshot).toEqual({'urn:local:abcd.ef': true});
    });
    it('set: two local urns with subpath', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'local', path: 'abcd'}, true, 'ef');
        sn.set({ type: 'local', path: 'qwer'}, {'x': 20, 'y': 40}, 'ty');
        expect(sn.snapshot).toEqual({'urn:local:abcd.ef': true, 'urn:local:qwer.ty': {'x': 20, 'y': 40}});
    });
    it('set: rtdb urn', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'rtdb', path: 'abcd/ef'}, true);
        expect(sn.snapshot).toEqual({'urn:rtdb:abcd/ef': true});
    });
    it('set: rtdb urn with subpath', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'rtdb', path: 'abcd/ef'}, true, 'gh');
        expect(sn.snapshot).toEqual({'urn:rtdb:abcd/ef/gh': true});
    });
    it('set: rtdb urn with nested subpath', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'rtdb', path: 'abcd/ef'}, true, 'gh.ui');
        expect(sn.snapshot).toEqual({'urn:rtdb:abcd/ef/gh/ui': true});
    });
    it('set: rtdb urn with nested subpath', () => {
        const sn = new SnapshotGenerator();
        sn.set({ type: 'rtdb', path: 'abcd/ef', limitToFirst: 30} as ResourceQuery<any>, true, 'gh.ui');
        expect(sn.snapshot).toEqual({'urn:rtdb:abcd/ef/gh/ui?limitToFirst=30': true});
    });
    it('setBatch', () => {
        const sn = new SnapshotGenerator();
        sn.setBatch({ type: 'rtdb', path: 'abcd/ef'} as ResourceQuery<any>, {'a.b': true, 'd.e': 'test'});
        expect(sn.snapshot).toEqual({'urn:rtdb:abcd/ef/a/b': true, 'urn:rtdb:abcd/ef/d/e': 'test'});
    });
});
