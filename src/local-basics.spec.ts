import * as LocalUrns from './local-basics-urns';
import localBasicSnapshoter from './local-basics-snapshoter';
import { navigated } from './actions/navigated';
import { userSignedIn } from './actions/user-signed-in';

describe('local-basics', async () => {
    it('loggedUserUrn', () => {
        expect(LocalUrns.loggedUserUrn()).toEqual({
            type: 'local',
            path: 'user',
        });
    });
    it('navigatedUrn', () => {
        expect(LocalUrns.navigatedUrn()).toEqual({
            type: 'local',
            path: 'navigated',
        });
    });
    it('local-basic-snapshoter - unknown event', () => {
        expect(localBasicSnapshoter({ type: 'xxxxx'}, null as any)).toEqual(null);
    });
    it('local-basic-snapshoter - navigated event', () => {
        expect(localBasicSnapshoter(navigated({ url: '/test', query: {}, params: {} }), null as any)).toEqual({
            'urn:local:navigated': { url: '/test', query: {}, params: {} },
        });
    });
    it('local-basic-snapshoter - no user event', () => {
        expect(localBasicSnapshoter(userSignedIn(), null as any)).toEqual({
            'urn:local:user': undefined,
        });
    });
    it('local-basic-snapshoter - user event', () => {
        expect(localBasicSnapshoter(userSignedIn('test', 'toro'), null as any)).toEqual({
            'urn:local:user': { uid: 'test', email: 'toro' },
        });
    });
});
