import { NavidatedActionPayload } from './actions/navigated';
import { UserSignedInActionPayload } from './actions/user-signed-in';

import { ResourceQuery } from './backend-interfaces';

export function navigatedUrn(): ResourceQuery<NavidatedActionPayload> {
    return { type: 'local', path: 'navigated' };
}

export function loggedUserUrn(): ResourceQuery<UserSignedInActionPayload> {
    return { type: 'local', path: 'user' };
}
