"use strict";

const admin = require("firebase-admin");
const utils = require('./utils');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');

const vars = utils.bootstrap(['-output']);
if (vars == null) {
    return;
}

exportFirestore();

const result = {};

async function exportFirestore() {
    const path = vars['-output'] + '/' + moment().format('YYYYMMDDHHmmssSSS') + (vars['-suffix'] != null ? '-' + vars['-suffix'] : '');
    console.log(path);
    fs.mkdirSync(path);
    const cols = await admin.firestore().getCollections();
    for (let i = 0; i < cols.length; ++i) {
        await exportCollection(cols[i]);
    }
    fs.writeFileSync(path + '/export.json', JSON.stringify(result) );
}

/**
 * @param col {FirebaseFirestore.CollectionReference}
 */
async function exportCollection(col) {
    process.stdout.write(col.path + ' ');
    let docs = null;
    while (true) {
        const qry = docs == null ? col : col.startAfter(docs[docs.length-1]);
        docs = (await qry.limit(500).get()).docs;
        if (docs == null || docs.length === 0) {
            break;
        }
        process.stdout.write(docs.length === 500 ? 'X' : 'x');
        for (let i = 0; i < docs.length; ++i) {
            const doc = docs[i];
            const data = doc.data();
            _.set(result, [col.path, doc.id], data);
        }    
    }
    console.log('');
}
