"use strict";

const admin = require("firebase-admin");
const utils = require('./utils');
const fs = require('fs');
const _ = require('lodash');


const vars = utils.bootstrap(['-input']);
if (vars == null) {
    return;
}

importFolder(vars['-input']);

async function importFolder(fld) {
    const files = fs.readdirSync(fld);
    for (let i = 0; i < files.length; ++i) {
        const file = files[i];
        const fullPath = fld + '/' + file;
        if (fs.statSync(fullPath).isDirectory()) {
            return;
        }
        await importFile(fullPath);
    }
    await utils.addToFirestoreBatch(null,null); // flush the queue
    console.log('');
}

async function importFile(path) {
    console.log(path);
    const fileContent = fs.readFileSync(path, {encoding: 'UTF-8'});
    const fileContentJson = JSON.parse(fileContent);
    const collections = _.keys(fileContentJson);
    for (let i = 0; i < collections.length; ++i) {
        await importCollection(collections[i], fileContentJson[collections[i]]);
    }
}

async function importCollection(colName, col) {
    process.stdout.write(colName + ' ');
    const docIds = _.keys(col);
    const colRef = admin.firestore().collection(colName);
    for (let i = 0; i < docIds.length; ++i) {
        await utils.addToFirestoreBatch(colRef.doc(docIds[i]), col[docIds[i]]);
    }
    if (utils.lengthOfFirestoreBatch() > 0) {
        process.stdout.write('>');
    }
    console.log('');
}