const admin = require("firebase-admin");

module.exports.bootstrap = (req) => {
    const vars = {};
    let currVar = '';
    process.argv.forEach(function (val) {
        if (val.startsWith('-')) {
            currVar = val;
        }
        else if (currVar !== '') {
            vars[currVar] = val;
        }
    });
    if (vars['-db'] == null) {
        console.error('need param -db');
        return null;
    }
    if (process.env.SERVICE_KEY == null) {
        console.error('need env SERVICE_KEY');
        return null;
    }
    const serviceAccount = require(process.env.SERVICE_KEY);
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: vars['-db']
    });
    for (let i = 0; i< req.length; ++i) {
        const param = req[i];
        if (vars[param] == null) {
            console.error('need param', param);
            return null;
        }
    }
    return vars;
}

let globalFirestoreBatch = null;
let globalFirestoreBatchLength = 0;

module.exports.addToFirestoreBatch = async (ref, data) => {
    if (globalFirestoreBatch == null && ref == null) {
        return;
    }
    if (globalFirestoreBatch == null) {
        globalFirestoreBatch = admin.firestore().batch();
        globalFirestoreBatchLength = 0;
    }
    if (ref != null) {
        globalFirestoreBatch.set(ref, data);
        ++globalFirestoreBatchLength;
    }
    if (ref == null || globalFirestoreBatchLength === 500) {
       await globalFirestoreBatch.commit();
       process.stdout.write(globalFirestoreBatchLength === 500 ? 'X' : 'x');
        globalFirestoreBatch = null;
    }
}

module.exports.lengthOfFirestoreBatch = () => {
    return globalFirestoreBatchLength;
}
