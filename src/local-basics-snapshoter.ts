import * as _ from 'lodash';
import { Action, UserSignedInActionType, NavigatedActionType, Walker } from '.';

export default function (action: Action, w: Walker): any {
    if (action.type.startsWith(UserSignedInActionType)) {
        return { 'urn:local:user': action.payload};
    }
    if (action.type.startsWith(NavigatedActionType)) {
        return { 'urn:local:navigated': action.payload};
    }
    return null;
}
