import { Backend } from './backend';

let backend: Backend;

export function getBackend(): Backend {
    if (backend == null) {
        backend = new Backend();
    }
    return backend;
}
