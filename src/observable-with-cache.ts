import * as _ from 'lodash';
import { Observable, Subscription } from 'rxjs';

export class ObservableWithCache<T> {
    private subscr?: Subscription;
    private obsr?: Observable<T>;
    val?: T;

    set source(obs: Observable<T>) {
        this.obsr = obs;
        this.unsubscribe();
        this.subscr = this.obsr.pipe(
        ). subscribe( (val) => {
            // console.log('StoreObservable - hit', val);
            return this.val = _.cloneDeep(val);
        });
    }
    constructor() {}
    unsubscribe() {
        if (this.subscr) {
            this.subscr.unsubscribe();
            this.subscr = undefined;
        }
    }
}
