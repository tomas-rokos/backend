import storeReducer from './store-reducer';

describe('store-reducer', async () => {
    it('unknown event', () => {
        expect(storeReducer(undefined, { type: 'unknown event' })).toEqual({});
    });
    it('empty store and event with snaphots', () => {
        expect(storeReducer({}, {
            type: 'custom event',
            snapshots: {
                'snapshoter_a': {
                    'urn:rtdb:xyz_a': 'string from A',
                    'urn:rtdb:xyz2_a': {
                        'first_a': 'second_a'
                    },
                },
                'snapshoter_b': {
                    'urn:local:xyz_b': 'string from B',
                    'urn:rtdb:xyz_b': {
                        'first_b': 'second_b'
                    },
                },
                'snapshoter_c': {
                    'urn:firestore:xyz_c': 'string from C',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                }
            },
        })).toEqual({
            'urn:local:xyz_b': 'string from B',
            'urn:local:xyz_c': {
                'first_c': 'second_c'
            },
        });
    });
    it('empty store and event with cache', () => {
        expect(storeReducer({}, {
            type: 'custom event',
            cache: {
                'urn:rtdb:xyz_s': 'string from S',
                'urn:local:xyz_s': {
                    'first_s': 'second_s'
                },
            },
        })).toEqual({
            'urn:rtdb:xyz_s': 'string from S',
            'urn:local:xyz_s': {
                'first_s': 'second_s'
            },
        });
    });
    it('empty store and event with snasphots and cache', () => {
        expect(storeReducer(undefined, {
            type: 'custom event',
            cache: {
                'urn:rtdb:xyz_s': 'string from S',
                'urn:local:xyz_s': {
                    'first_s': 'second_s'
                },
            },
            snapshots: {
                'snapshoter_a': {
                    'urn:rtdb:xyz_a': 'string from A',
                    'urn:rtdb:xyz2_a': {
                        'first_a': 'second_a'
                    },
                },
                'snapshoter_b': {
                    'urn:local:xyz_b': 'string from B',
                    'urn:rtdb:xyz_b': {
                        'first_b': 'second_b'
                    },
                },
                'snapshoter_c': {
                    'urn:firestore:xyz_c': 'string from C',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                }
            },
        })).toEqual({
            'urn:rtdb:xyz_s': 'string from S',
            'urn:local:xyz_s': {
                'first_s': 'second_s'
            },
            'urn:local:xyz_b': 'string from B',
            'urn:local:xyz_c': {
                'first_c': 'second_c'
            },
        });
    });
    it('filled store and event with snasphots and cache without overlap', () => {
        expect(storeReducer({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e'
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        }, {
            type: 'custom event',
            cache: {
                'urn:rtdb:xyz_s': 'string from S',
                'urn:local:xyz_s': {
                    'first_s': 'second_s'
                },
            },
            snapshots: {
                'snapshoter_a': {
                    'urn:rtdb:xyz_a': 'string from A',
                    'urn:rtdb:xyz2_a': {
                        'first_a': 'second_a'
                    },
                },
                'snapshoter_b': {
                    'urn:local:xyz_b': 'string from B',
                    'urn:rtdb:xyz_b': {
                        'first_b': 'second_b'
                    },
                },
                'snapshoter_c': {
                    'urn:firestore:xyz_c': 'string from C',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                }
            },
        })).toEqual({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e'
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
            'urn:rtdb:xyz_s': 'string from S',
            'urn:local:xyz_s': {
                'first_s': 'second_s'
            },
            'urn:local:xyz_b': 'string from B',
            'urn:local:xyz_c': {
                'first_c': 'second_c'
            },
        });
    });
    it('filled store and event with snasphots and cache with overlap', () => {
        expect(storeReducer({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e'
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        }, {
            type: 'custom event',
            cache: {
                'urn:rtdb:xyz_e': 'string from S',
                'urn:local:xyz_s': {
                    'first_s': 'second_s'
                },
            },
            snapshots: {
                'snapshoter_a': {
                    'urn:rtdb:xyz_a': 'string from A',
                    'urn:rtdb:xyz2_a': {
                        'first_a': 'second_a'
                    },
                },
                'snapshoter_b': {
                    'urn:local:xyz_b': 'string from B',
                    'urn:rtdb:xyz_b': {
                        'first_b': 'second_b'
                    },
                },
                'snapshoter_c': {
                    'urn:firestore:xyz_c': 'string from C',
                    'urn:local:xyz_e2': {
                        'first_c': 'second_c'
                    },
                }
            },
        })).toEqual({
            'urn:rtdb:xyz_e': 'string from S',
            'urn:local:xyz_e': {
                'first_e': 'second_e'
            },
            'urn:local:xyz_e2': {
                'first_c': 'second_c'
            },
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
            'urn:local:xyz_s': {
                'first_s': 'second_s'
            },
            'urn:local:xyz_b': 'string from B',
        });
    });
    it('filled store and event with cache with overlap with fewer keys', () => {
        expect(storeReducer({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e',
                'third_e': 'forth_e',
                'fifth_e': 'sixth_e',
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        }, {
            type: 'custom event',
            cache: {
                'urn:local:xyz_e': {
                    'third_e': 'seventh_e',
                }
            },
        })).toEqual({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'third_e': 'seventh_e',
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        });
    });
    it('filled store and event with cache to nested element', () => {
        expect(storeReducer({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e',
                'third_e': {
                    'a': 'aa',
                    'b': 'bb',
                    'c': 'cc',
                },
                'fifth_e': 'sixth_e',
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        }, {
            type: 'custom event',
            cache: {
                'urn:local:xyz_e.third_e.b': 'bbb'
            },
        })).toEqual({
            'urn:rtdb:xyz_e': 'string from E',
            'urn:local:xyz_e': {
                'first_e': 'second_e',
                'third_e': {
                    'a': 'aa',
                    'b': 'bbb',
                    'c': 'cc',
                },
                'fifth_e': 'sixth_e',
            },
            'urn:local:xyz_e2': 'string from e2',
            'urn:local:xyz_e3': {
                'first_e3': 'second_e3'
            },
        });
    });
});
