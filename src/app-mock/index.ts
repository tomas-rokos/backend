import * as firebaseNighlight from 'firebase-nightlight';

export default function setAppMock(stub: any) {
    const mock = new firebaseNighlight.Mock(stub);
    return mock.initializeApp({});
}
