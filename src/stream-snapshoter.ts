import * as _ from 'lodash';

import { EventLike, Walker } from './backend-interfaces';

let d21loggingSession: any = null;

export default async function (e: EventLike, w: Walker) {
    let sessionEvent = null;
    if (d21loggingSession == null) {
        sessionEvent = await doTheLog({
            type: 'session:start',
            payload: sessionPayload(),
        }, w, true);
    }
    const logEvent = await doTheLog(e, w);
    return sessionEvent ? _.merge(sessionEvent, logEvent) : logEvent;
}

function sessionPayload() {
    return {
        's': {
            'w': window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            'h': window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
        },
    };
}

async function doTheLog(e: EventLike, w: Walker, thisIsSession = false) {
    const dt = new Date();
    let res = dt.getUTCFullYear().toString();
    if (dt.getUTCMonth() < 9) {
        res += '0';
    }
    res += (dt.getUTCMonth() + 1).toString();
    if (dt.getUTCDate() < 10) {
        res += '0';
    }
    res += dt.getUTCDate();
    const newId = await w.newKey({
        type: 'rtdb',
        path: 'stream/' + res,
    });
    if (thisIsSession) {
        d21loggingSession = newId;
    }
    return {
        ['urn:rtdb:stream/' + res + '/' + newId]: {
            't': {
                't': Date.now(),
                'o': new Date().getTimezoneOffset(),
            },
            'a': e.type,
            'd': e.payload,
            's': d21loggingSession || '',
        },
    };
}
