import * as _ from 'lodash';

import { EventLike } from './backend-interfaces';

export default function(state: any, action: EventLike): any {
    if (action == null) {
        return state;
    }
    const locals: {[key: string]: any} = {};
    const snapshots = _.concat(_.values(action.snapshots), [action.snapshot]);
    _.forEach(snapshots, (singleSnapshot) => {
        _.forIn(singleSnapshot, (dataVal, dataKey) => {
            if (dataKey.startsWith('urn:local:') === true) {
                locals[dataKey] = dataVal;
            }
        });
    });
    _.forIn(action['cache'], (dataVal, dataKey) => {
        locals[dataKey] = dataVal;
    });
    const finalState = state ? _.cloneDeep(state) : {};
    _.forIn(locals, (localVal, localKey) => {
        const parts = localKey.split('.');
        _.set(finalState, parts, localVal);
    });
    return finalState;
}
