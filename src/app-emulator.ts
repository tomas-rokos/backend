import * as firebase from '@firebase/testing';

export default async function (databaseName: string, rules: string, auth?: { uid: string }, stub: object|null = null ) {
    // this code needs to live here to be completely removed in online compilations
    const mock = firebase.initializeTestApp({
        databaseName,
        auth,
    });
    await firebase.loadDatabaseRules({
        databaseName,
        rules,
    });
    const admin = firebase.initializeAdminApp({databaseName});
    admin.database().ref().set(stub);
    return mock;
}
