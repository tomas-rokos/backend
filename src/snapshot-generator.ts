import * as _ from 'lodash';

import { resourceHandlers } from './backend';
import { ResourceQuery } from './backend-interfaces';

export class SnapshotGenerator {
    snapshot = {};
    static resourceQueryToString(q: ResourceQuery<any>, subpath = ''): string {
        return resourceHandlers[q.type].toString(q, subpath);
    }
    set<T>(q: ResourceQuery<T>, data: any, subpath = '') {
        const key = SnapshotGenerator.resourceQueryToString(q, subpath);
        this.snapshot[key] = data;
    }
    setBatch<T>(q: ResourceQuery<T>, data: { [subpath: string]: any }) {
        _.forIn(data, (val, subpath) => {
            this.set(q, val, subpath);
        });
    }
}
