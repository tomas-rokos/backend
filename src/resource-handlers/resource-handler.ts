import { ResourceQuery, EventLike, Walker } from '../index';

export abstract class ResourceHandler {
    abstract toString<T>(q: ResourceQuery<T>, subPath?: string): string;
    abstract ensureLoaded<T>(q: ResourceQuery<T>, backend: Walker): Promise<T>;
    abstract set(obj: object): Promise<boolean>;
    abstract newKey(q: ResourceQuery<string>): Promise<string | null>;
}
