import * as _ from 'lodash';
import 'firebase/database';

import { ResourceQuery, EventLike } from '../backend-interfaces';
import { ResourceHandler } from './resource-handler';

export class Local extends ResourceHandler {
    toString<T>(q: ResourceQuery<T>, subPath?: string): string {
        return 'urn:' + q.type + ':' + q.path + (subPath && subPath.length > 0 ? '.' + subPath : '');
    }
    ensureLoaded<T>(q: ResourceQuery<T>, backend: any): Promise<T> { return Promise.resolve(null) as any; }
    set(obj: object): Promise<boolean> { return null as any; }
    newKey(q: ResourceQuery<string>): Promise<string> {
        return Promise.resolve(Date.now().toString());
    }
}
