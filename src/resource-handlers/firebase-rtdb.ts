import * as _ from 'lodash';
import 'firebase/database';

import { getApp } from '../app';
import { ResourceQuery, FirebaseRTDBQuery, EventLike, Walker } from '../backend-interfaces';
import { ResourceHandler } from './resource-handler';

export class FirebaseRTDB extends ResourceHandler {
    toString<T>(q: FirebaseRTDBQuery<T>, subPath?: string): string {
        const params = [];
        if (q.equalTo) {
            params.push('equalTo=' + q.equalTo);
        }
        if (q.limitToFirst) {
            params.push('limitToFirst=' + q.limitToFirst);
        }
        if (q.limitToLast) {
            params.push('limitToLast=' + q.limitToLast);
        }
        if (q.orderByChild) {
            params.push('orderByChild=' + q.orderByChild);
        }
        const base = 'urn:' + q.type + ':' + q.path + (subPath && subPath.length > 0 ? '/' + subPath.split('.').join('/') : '');
        return params.length > 0 ? base + '?' + params.join('&') : base;
    }
    ensureLoaded<T>(q: FirebaseRTDBQuery<T>, backend: Walker): Promise<T> {
        const storeKey = this.toString(q);
        const result = {
            'type': 'RTDB sync: ' + storeKey,
            'cache': {
                [storeKey]: null,
            }
        };
        backend.dispatch(result);

        let firstResolver: any = null;
        // the stream of updates is propagated to backend
        const cq = q as FirebaseRTDBQuery<any>;
        let qry: firebase.database.Query = getApp().database().ref(cq.path);
        if (q.equalTo) {
            qry = qry.equalTo(q.equalTo);
        }
        if (q.limitToFirst) {
            qry = qry.limitToFirst(q.limitToFirst);
        }
        if (q.limitToLast) {
            qry = qry.limitToLast(q.limitToLast);
        }
        if (q.orderByChild) {
            qry = qry.orderByChild(q.orderByChild);
        }
        const subscription = qry.on('value', (snapshot) => {
            const val = snapshot ? snapshot.val() : null;
            backend.dispatch({
                'type': 'RTDB value: ' + storeKey,
                'cache': {
                    [storeKey]: val,
                }
            });
            if (firstResolver) {
                firstResolver(val);
                firstResolver = null;
            }
        });
        // returned promise makes the ensuredLoaded to notify upon data arrival
        return new Promise<T>((resolve) => {
            firstResolver = resolve;
        });
    }
    set(obj: object): Promise<boolean> {
        return getApp().database().ref('/').update(obj);
    }
    newKey(q: ResourceQuery<string>): Promise<string | null> {
        return Promise.resolve(getApp().database().ref(q.path).push().key);
    }
}
