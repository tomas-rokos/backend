

export interface ResourceQuery<T> {
    type: 'local' | 'rtdb' | 'firestore' | 'storage';
    path: string;
}

export interface FirebaseRTDBQuery<T> extends ResourceQuery<T> {
    type: 'rtdb';
    limitToFirst?: number;
    limitToLast?: number;
    orderByChild?: string;
    equalTo?: any;
}

export interface EventLike {
    type: string;
    payload?: any;
    snapshot?: Object;          // embedded snapshot directly from event
    snapshoter?: ((e: EventLike, w: Walker) => Promise<any> | any);
    snapshots?: {
        [key: string]: {        // name of snapshoter
            [key: string]: any; // update batch
        };
    };
    cache?: {                   // just send data to local store
        [key: string]: any;
    };
}

export type ThunkLike<T> = (w: Walker) => Promise<T>;

export interface Walker {
    newKey(q: ResourceQuery<string>): Promise<string | null>;
    get<T>(q: ResourceQuery<T> | string): T;
    getAsync<T>(q: ResourceQuery<T> | string): Promise<T>;
    dispatch<T>(event: EventLike | ThunkLike<T>): Promise<T>;
}
