import * as _ from 'lodash';
import { useEffect, useState } from 'react';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { Walker } from '../src/backend-interfaces';

import { getBackend } from '../src/backend-singleton';

export function useBackend<P>( mapBackendToProps: (w: Walker, propsIn: any) => P, props?: any) {
    const [value, setValue] = useState(null);

    useEffect(() => {
        const subscription = getBackend().state().pipe(
            map( (w) => mapBackendToProps(w, props)),
            map( (val) => {
                if (_.isEqual(val, value) === false) {
                    setValue(val);
                }
                return val;
            })
        ).subscribe();

        return () => subscription.unsubscribe();
    });

    return value;
}
