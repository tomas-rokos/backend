import * as _ from 'lodash';
import * as React from 'react';

import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { Walker } from '../src/backend-interfaces';

import { getBackend } from '../src/backend-singleton';

export function connectToBackend<P>(mapBackendToProps: (w: Walker, propsIn: any) => P) {
    return function (WrappedComponent: any) {
        return class WrapperComponent extends React.Component {
            walker?: Subscription;
            mounted = false;
            constructor(props: any) {
                super(props);
                this.componentWillReceiveProps(props);
            }
            componentDidMount() {
                this.mounted = true;
            }
            componentWillUnmount() {
                this.mounted = false;
                this.unsubscribe();
            }
            unsubscribe() {
                if (this.walker != null) {
                    this.walker.unsubscribe();
                    this.walker = undefined;
                }
            }
            componentWillReceiveProps(props: any) {
                this.unsubscribe();
                this.walker = getBackend().state().pipe(
                    map( (w) => mapBackendToProps(w, props)),
                    map ( (val: P) => {
                        if (this.mounted) {
                            this.setState(val);
                        } else {
                            this.state = val;
                        }
                    })
                ).subscribe();
            }
            render() {
                return React.createElement(WrappedComponent, _.merge({}, this.props, this.state));
            }
        };
    };
}
