import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Router, ActivationEnd, NavigationEnd, NavigationStart } from '@angular/router';
import { Backend } from '../src/backend';
import { navigated } from '../src/actions/navigated';

@Injectable({
   providedIn: 'root',
})
export class BackendService extends Backend {
    constructor(
        router: Router,
    ) {
        super();
        if (router) {
            let routeData = null;
            router.events.subscribe (( val) => {
                if (val instanceof ActivationEnd) {
                    // console.log('activationEnd');
                    // const path = [];
                    // _.forEach(val.snapshot.url, (seg) => {
                    //     path.push( {path: seg.path, params: _.cloneDeep(seg.parameters)});
                    // });
                    routeData = _.merge(routeData,  {
                        params: _.cloneDeep(val.snapshot.params),
                        query: _.cloneDeep(val.snapshot.queryParams),
                        // config: _.cloneDeep(val.snapshot.routeConfig.path),
                        // path: path,
                    });
                } else if (val instanceof NavigationEnd) {
                    // console.log('navigationEnd', val);
                    routeData.url = val.urlAfterRedirects;
                    this.dispatch(navigated(routeData));
                } else if (val instanceof NavigationStart) {
                    // console.log('navigationStart');
                    routeData = null;
                }
            });
        }

    }
}
