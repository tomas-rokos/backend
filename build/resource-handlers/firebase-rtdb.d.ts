import 'firebase/database';
import { ResourceQuery, FirebaseRTDBQuery, Walker } from '../backend-interfaces';
import { ResourceHandler } from './resource-handler';
export declare class FirebaseRTDB extends ResourceHandler {
    toString<T>(q: FirebaseRTDBQuery<T>, subPath?: string): string;
    ensureLoaded<T>(q: FirebaseRTDBQuery<T>, backend: Walker): Promise<T>;
    set(obj: object): Promise<boolean>;
    newKey(q: ResourceQuery<string>): Promise<string | null>;
}
