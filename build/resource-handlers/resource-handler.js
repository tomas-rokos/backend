(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ResourceHandler = /** @class */ (function () {
        function ResourceHandler() {
        }
        return ResourceHandler;
    }());
    exports.ResourceHandler = ResourceHandler;
});
//# sourceMappingURL=resource-handler.js.map