var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "firebase/database", "./resource-handler"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    require("firebase/database");
    var resource_handler_1 = require("./resource-handler");
    var Local = /** @class */ (function (_super) {
        __extends(Local, _super);
        function Local() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Local.prototype.toString = function (q, subPath) {
            return 'urn:' + q.type + ':' + q.path + (subPath && subPath.length > 0 ? '.' + subPath : '');
        };
        Local.prototype.ensureLoaded = function (q, backend) { return Promise.resolve(null); };
        Local.prototype.set = function (obj) { return null; };
        Local.prototype.newKey = function (q) {
            return Promise.resolve(Date.now().toString());
        };
        return Local;
    }(resource_handler_1.ResourceHandler));
    exports.Local = Local;
});
//# sourceMappingURL=local.js.map