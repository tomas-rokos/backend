import 'firebase/database';
import { ResourceQuery } from '../backend-interfaces';
import { ResourceHandler } from './resource-handler';
export declare class Local extends ResourceHandler {
    toString<T>(q: ResourceQuery<T>, subPath?: string): string;
    ensureLoaded<T>(q: ResourceQuery<T>, backend: any): Promise<T>;
    set(obj: object): Promise<boolean>;
    newKey(q: ResourceQuery<string>): Promise<string>;
}
