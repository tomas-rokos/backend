var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "firebase/database", "../app", "./resource-handler"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    require("firebase/database");
    var app_1 = require("../app");
    var resource_handler_1 = require("./resource-handler");
    var FirebaseRTDB = /** @class */ (function (_super) {
        __extends(FirebaseRTDB, _super);
        function FirebaseRTDB() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        FirebaseRTDB.prototype.toString = function (q, subPath) {
            var params = [];
            if (q.equalTo) {
                params.push('equalTo=' + q.equalTo);
            }
            if (q.limitToFirst) {
                params.push('limitToFirst=' + q.limitToFirst);
            }
            if (q.limitToLast) {
                params.push('limitToLast=' + q.limitToLast);
            }
            if (q.orderByChild) {
                params.push('orderByChild=' + q.orderByChild);
            }
            var base = 'urn:' + q.type + ':' + q.path + (subPath && subPath.length > 0 ? '/' + subPath.split('.').join('/') : '');
            return params.length > 0 ? base + '?' + params.join('&') : base;
        };
        FirebaseRTDB.prototype.ensureLoaded = function (q, backend) {
            var _a;
            var storeKey = this.toString(q);
            var result = {
                'type': 'RTDB sync: ' + storeKey,
                'cache': (_a = {},
                    _a[storeKey] = null,
                    _a)
            };
            backend.dispatch(result);
            var firstResolver = null;
            // the stream of updates is propagated to backend
            var cq = q;
            var qry = app_1.getApp().database().ref(cq.path);
            if (q.equalTo) {
                qry = qry.equalTo(q.equalTo);
            }
            if (q.limitToFirst) {
                qry = qry.limitToFirst(q.limitToFirst);
            }
            if (q.limitToLast) {
                qry = qry.limitToLast(q.limitToLast);
            }
            if (q.orderByChild) {
                qry = qry.orderByChild(q.orderByChild);
            }
            var subscription = qry.on('value', function (snapshot) {
                var _a;
                var val = snapshot ? snapshot.val() : null;
                backend.dispatch({
                    'type': 'RTDB value: ' + storeKey,
                    'cache': (_a = {},
                        _a[storeKey] = val,
                        _a)
                });
                if (firstResolver) {
                    firstResolver(val);
                    firstResolver = null;
                }
            });
            // returned promise makes the ensuredLoaded to notify upon data arrival
            return new Promise(function (resolve) {
                firstResolver = resolve;
            });
        };
        FirebaseRTDB.prototype.set = function (obj) {
            return app_1.getApp().database().ref('/').update(obj);
        };
        FirebaseRTDB.prototype.newKey = function (q) {
            return Promise.resolve(app_1.getApp().database().ref(q.path).push().key);
        };
        return FirebaseRTDB;
    }(resource_handler_1.ResourceHandler));
    exports.FirebaseRTDB = FirebaseRTDB;
});
//# sourceMappingURL=firebase-rtdb.js.map