import { EventLike, Walker } from './backend-interfaces';
export default function (e: EventLike, w: Walker): Promise<{
    [x: string]: {
        't': {
            't': number;
            'o': number;
        };
        'a': string;
        'd': any;
        's': any;
    };
}>;
