(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var gApp;
    function setApp(app) {
        gApp = app;
    }
    exports.setApp = setApp;
    function getApp() {
        return gApp;
    }
    exports.getApp = getApp;
});
//# sourceMappingURL=app.js.map