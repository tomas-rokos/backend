(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _ = require("lodash");
    function default_1(state, action) {
        if (action == null) {
            return state;
        }
        var locals = {};
        var snapshots = _.concat(_.values(action.snapshots), [action.snapshot]);
        _.forEach(snapshots, function (singleSnapshot) {
            _.forIn(singleSnapshot, function (dataVal, dataKey) {
                if (dataKey.startsWith('urn:local:') === true) {
                    locals[dataKey] = dataVal;
                }
            });
        });
        _.forIn(action['cache'], function (dataVal, dataKey) {
            locals[dataKey] = dataVal;
        });
        var finalState = state ? _.cloneDeep(state) : {};
        _.forIn(locals, function (localVal, localKey) {
            var parts = localKey.split('.');
            _.set(finalState, parts, localVal);
        });
        return finalState;
    }
    exports.default = default_1;
});
//# sourceMappingURL=store-reducer.js.map