(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "."], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _1 = require(".");
    function default_1(action, w) {
        if (action.type.startsWith(_1.UserSignedInActionType)) {
            return { 'urn:local:user': action.payload };
        }
        if (action.type.startsWith(_1.NavigatedActionType)) {
            return { 'urn:local:navigated': action.payload };
        }
        return null;
    }
    exports.default = default_1;
});
//# sourceMappingURL=local-basics-snapshoter.js.map