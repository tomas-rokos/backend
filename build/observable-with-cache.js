(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _ = require("lodash");
    var ObservableWithCache = /** @class */ (function () {
        function ObservableWithCache() {
        }
        Object.defineProperty(ObservableWithCache.prototype, "source", {
            set: function (obs) {
                var _this = this;
                this.obsr = obs;
                this.unsubscribe();
                this.subscr = this.obsr.pipe().subscribe(function (val) {
                    // console.log('StoreObservable - hit', val);
                    return _this.val = _.cloneDeep(val);
                });
            },
            enumerable: true,
            configurable: true
        });
        ObservableWithCache.prototype.unsubscribe = function () {
            if (this.subscr) {
                this.subscr.unsubscribe();
                this.subscr = undefined;
            }
        };
        return ObservableWithCache;
    }());
    exports.ObservableWithCache = ObservableWithCache;
});
//# sourceMappingURL=observable-with-cache.js.map