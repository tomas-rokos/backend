import { NavidatedActionPayload } from './actions/navigated';
import { UserSignedInActionPayload } from './actions/user-signed-in';
import { ResourceQuery } from './backend-interfaces';
export declare function navigatedUrn(): ResourceQuery<NavidatedActionPayload>;
export declare function loggedUserUrn(): ResourceQuery<UserSignedInActionPayload>;
