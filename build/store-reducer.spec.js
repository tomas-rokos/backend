var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./store-reducer"], factory);
    }
})(function (require, exports) {
    "use strict";
    var _this = this;
    Object.defineProperty(exports, "__esModule", { value: true });
    var store_reducer_1 = require("./store-reducer");
    describe('store-reducer', function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            it('unknown event', function () {
                expect(store_reducer_1.default(undefined, { type: 'unknown event' })).toEqual({});
            });
            it('empty store and event with snaphots', function () {
                expect(store_reducer_1.default({}, {
                    type: 'custom event',
                    snapshots: {
                        'snapshoter_a': {
                            'urn:rtdb:xyz_a': 'string from A',
                            'urn:rtdb:xyz2_a': {
                                'first_a': 'second_a'
                            },
                        },
                        'snapshoter_b': {
                            'urn:local:xyz_b': 'string from B',
                            'urn:rtdb:xyz_b': {
                                'first_b': 'second_b'
                            },
                        },
                        'snapshoter_c': {
                            'urn:firestore:xyz_c': 'string from C',
                            'urn:local:xyz_c': {
                                'first_c': 'second_c'
                            },
                        }
                    },
                })).toEqual({
                    'urn:local:xyz_b': 'string from B',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                });
            });
            it('empty store and event with cache', function () {
                expect(store_reducer_1.default({}, {
                    type: 'custom event',
                    cache: {
                        'urn:rtdb:xyz_s': 'string from S',
                        'urn:local:xyz_s': {
                            'first_s': 'second_s'
                        },
                    },
                })).toEqual({
                    'urn:rtdb:xyz_s': 'string from S',
                    'urn:local:xyz_s': {
                        'first_s': 'second_s'
                    },
                });
            });
            it('empty store and event with snasphots and cache', function () {
                expect(store_reducer_1.default(undefined, {
                    type: 'custom event',
                    cache: {
                        'urn:rtdb:xyz_s': 'string from S',
                        'urn:local:xyz_s': {
                            'first_s': 'second_s'
                        },
                    },
                    snapshots: {
                        'snapshoter_a': {
                            'urn:rtdb:xyz_a': 'string from A',
                            'urn:rtdb:xyz2_a': {
                                'first_a': 'second_a'
                            },
                        },
                        'snapshoter_b': {
                            'urn:local:xyz_b': 'string from B',
                            'urn:rtdb:xyz_b': {
                                'first_b': 'second_b'
                            },
                        },
                        'snapshoter_c': {
                            'urn:firestore:xyz_c': 'string from C',
                            'urn:local:xyz_c': {
                                'first_c': 'second_c'
                            },
                        }
                    },
                })).toEqual({
                    'urn:rtdb:xyz_s': 'string from S',
                    'urn:local:xyz_s': {
                        'first_s': 'second_s'
                    },
                    'urn:local:xyz_b': 'string from B',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                });
            });
            it('filled store and event with snasphots and cache without overlap', function () {
                expect(store_reducer_1.default({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e'
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                }, {
                    type: 'custom event',
                    cache: {
                        'urn:rtdb:xyz_s': 'string from S',
                        'urn:local:xyz_s': {
                            'first_s': 'second_s'
                        },
                    },
                    snapshots: {
                        'snapshoter_a': {
                            'urn:rtdb:xyz_a': 'string from A',
                            'urn:rtdb:xyz2_a': {
                                'first_a': 'second_a'
                            },
                        },
                        'snapshoter_b': {
                            'urn:local:xyz_b': 'string from B',
                            'urn:rtdb:xyz_b': {
                                'first_b': 'second_b'
                            },
                        },
                        'snapshoter_c': {
                            'urn:firestore:xyz_c': 'string from C',
                            'urn:local:xyz_c': {
                                'first_c': 'second_c'
                            },
                        }
                    },
                })).toEqual({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e'
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                    'urn:rtdb:xyz_s': 'string from S',
                    'urn:local:xyz_s': {
                        'first_s': 'second_s'
                    },
                    'urn:local:xyz_b': 'string from B',
                    'urn:local:xyz_c': {
                        'first_c': 'second_c'
                    },
                });
            });
            it('filled store and event with snasphots and cache with overlap', function () {
                expect(store_reducer_1.default({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e'
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                }, {
                    type: 'custom event',
                    cache: {
                        'urn:rtdb:xyz_e': 'string from S',
                        'urn:local:xyz_s': {
                            'first_s': 'second_s'
                        },
                    },
                    snapshots: {
                        'snapshoter_a': {
                            'urn:rtdb:xyz_a': 'string from A',
                            'urn:rtdb:xyz2_a': {
                                'first_a': 'second_a'
                            },
                        },
                        'snapshoter_b': {
                            'urn:local:xyz_b': 'string from B',
                            'urn:rtdb:xyz_b': {
                                'first_b': 'second_b'
                            },
                        },
                        'snapshoter_c': {
                            'urn:firestore:xyz_c': 'string from C',
                            'urn:local:xyz_e2': {
                                'first_c': 'second_c'
                            },
                        }
                    },
                })).toEqual({
                    'urn:rtdb:xyz_e': 'string from S',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e'
                    },
                    'urn:local:xyz_e2': {
                        'first_c': 'second_c'
                    },
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                    'urn:local:xyz_s': {
                        'first_s': 'second_s'
                    },
                    'urn:local:xyz_b': 'string from B',
                });
            });
            it('filled store and event with cache with overlap with fewer keys', function () {
                expect(store_reducer_1.default({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e',
                        'third_e': 'forth_e',
                        'fifth_e': 'sixth_e',
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                }, {
                    type: 'custom event',
                    cache: {
                        'urn:local:xyz_e': {
                            'third_e': 'seventh_e',
                        }
                    },
                })).toEqual({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'third_e': 'seventh_e',
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                });
            });
            it('filled store and event with cache to nested element', function () {
                expect(store_reducer_1.default({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e',
                        'third_e': {
                            'a': 'aa',
                            'b': 'bb',
                            'c': 'cc',
                        },
                        'fifth_e': 'sixth_e',
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                }, {
                    type: 'custom event',
                    cache: {
                        'urn:local:xyz_e.third_e.b': 'bbb'
                    },
                })).toEqual({
                    'urn:rtdb:xyz_e': 'string from E',
                    'urn:local:xyz_e': {
                        'first_e': 'second_e',
                        'third_e': {
                            'a': 'aa',
                            'b': 'bbb',
                            'c': 'cc',
                        },
                        'fifth_e': 'sixth_e',
                    },
                    'urn:local:xyz_e2': 'string from e2',
                    'urn:local:xyz_e3': {
                        'first_e3': 'second_e3'
                    },
                });
            });
            return [2 /*return*/];
        });
    }); });
});
//# sourceMappingURL=store-reducer.spec.js.map