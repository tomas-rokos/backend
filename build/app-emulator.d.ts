export default function (databaseName: string, rules: string, auth?: {
    uid: string;
}, stub?: object | null): Promise<import("firebase").app.App>;
