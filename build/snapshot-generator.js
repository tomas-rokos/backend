(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "./backend"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _ = require("lodash");
    var backend_1 = require("./backend");
    var SnapshotGenerator = /** @class */ (function () {
        function SnapshotGenerator() {
            this.snapshot = {};
        }
        SnapshotGenerator.resourceQueryToString = function (q, subpath) {
            if (subpath === void 0) { subpath = ''; }
            return backend_1.resourceHandlers[q.type].toString(q, subpath);
        };
        SnapshotGenerator.prototype.set = function (q, data, subpath) {
            if (subpath === void 0) { subpath = ''; }
            var key = SnapshotGenerator.resourceQueryToString(q, subpath);
            this.snapshot[key] = data;
        };
        SnapshotGenerator.prototype.setBatch = function (q, data) {
            var _this = this;
            _.forIn(data, function (val, subpath) {
                _this.set(q, val, subpath);
            });
        };
        return SnapshotGenerator;
    }());
    exports.SnapshotGenerator = SnapshotGenerator;
});
//# sourceMappingURL=snapshot-generator.js.map