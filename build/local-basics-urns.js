(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function navigatedUrn() {
        return { type: 'local', path: 'navigated' };
    }
    exports.navigatedUrn = navigatedUrn;
    function loggedUserUrn() {
        return { type: 'local', path: 'user' };
    }
    exports.loggedUserUrn = loggedUserUrn;
});
//# sourceMappingURL=local-basics-urns.js.map