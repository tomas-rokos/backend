import * as firebase from 'firebase/app';
export declare function setApp(app: firebase.app.App): void;
export declare function getApp(): firebase.app.App;
