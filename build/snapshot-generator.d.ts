import { ResourceQuery } from './backend-interfaces';
export declare class SnapshotGenerator {
    snapshot: {};
    static resourceQueryToString(q: ResourceQuery<any>, subpath?: string): string;
    set<T>(q: ResourceQuery<T>, data: any, subpath?: string): void;
    setBatch<T>(q: ResourceQuery<T>, data: {
        [subpath: string]: any;
    }): void;
}
