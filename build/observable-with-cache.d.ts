import { Observable } from 'rxjs';
export declare class ObservableWithCache<T> {
    private subscr?;
    private obsr?;
    val?: T;
    source: Observable<T>;
    constructor();
    unsubscribe(): void;
}
