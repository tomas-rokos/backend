(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./backend"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var backend_1 = require("./backend");
    var backend;
    function getBackend() {
        if (backend == null) {
            backend = new backend_1.Backend();
        }
        return backend;
    }
    exports.getBackend = getBackend;
});
//# sourceMappingURL=backend-singleton.js.map