(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.NavigatedActionType = '@navigated';
    function navigated(data) {
        return {
            type: exports.NavigatedActionType + ' - ' + data.url,
            payload: data,
        };
    }
    exports.navigated = navigated;
});
//# sourceMappingURL=navigated.js.map