export interface Action {
    type: string;
    payload?: Object;
    snapshot?: Object;
}
