import { Walker, ResourceQuery } from '..';
export declare function generalUpdateAction(urn: ResourceQuery<any>, deltas: {
    [key: string]: any;
}, title?: string): (w: Walker) => Promise<{
    type: string;
    payload: {
        path: string;
        deltas: {
            [key: string]: any;
        };
    };
    snapshot: {};
}>;
