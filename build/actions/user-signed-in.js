(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.UserSignedInActionType = '@user-signed-in';
    function userSignedIn(uid, email) {
        return uid != null && email != null ?
            { type: exports.UserSignedInActionType + ' - ' + email, payload: { uid: uid, email: email } }
            :
                { type: exports.UserSignedInActionType + ' - NO' };
    }
    exports.userSignedIn = userSignedIn;
});
//# sourceMappingURL=user-signed-in.js.map