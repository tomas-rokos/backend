import { Action } from './action';
export declare const NavigatedActionType = "@navigated";
export interface NavidatedActionPayload {
    url: string;
    params: any;
    query: any;
}
export declare function navigated(data: NavidatedActionPayload): Action;
