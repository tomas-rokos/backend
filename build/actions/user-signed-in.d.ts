import { Action } from './action';
export declare const UserSignedInActionType = "@user-signed-in";
export interface UserSignedInActionPayload {
    uid: string;
    email: string;
}
export declare function userSignedIn(uid?: string, email?: string): Action;
