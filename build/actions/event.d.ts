export declare const EventActionType = "@event";
export declare function event(name: string, value?: any, customType?: null): {
    type: string | null;
    payload: {
        name: string;
        value: any;
    };
    cache: {
        [x: string]: any;
    };
};
