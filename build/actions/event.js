(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "moment"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var moment_1 = require("moment");
    exports.EventActionType = '@event';
    function event(name, value, customType) {
        if (value === void 0) { value = moment_1.now(); }
        if (customType === void 0) { customType = null; }
        var _a;
        return {
            type: customType ? customType : exports.EventActionType + ' - ' + name,
            payload: { name: name, value: value },
            cache: (_a = {}, _a['urn:local:' + name] = value, _a),
        };
    }
    exports.event = event;
});
//# sourceMappingURL=event.js.map