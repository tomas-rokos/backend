(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./backend", "./snapshot-generator", "./app", "./observable-with-cache", "./actions/navigated", "./actions/user-signed-in", "./actions/event", "./actions/general-update", "./local-basics-urns"], factory);
    }
})(function (require, exports) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    Object.defineProperty(exports, "__esModule", { value: true });
    var backend_1 = require("./backend");
    exports.Backend = backend_1.Backend;
    __export(require("./snapshot-generator"));
    __export(require("./app"));
    __export(require("./observable-with-cache"));
    __export(require("./actions/navigated"));
    __export(require("./actions/user-signed-in"));
    __export(require("./actions/event"));
    __export(require("./actions/general-update"));
    __export(require("./local-basics-urns"));
});
//# sourceMappingURL=index.js.map