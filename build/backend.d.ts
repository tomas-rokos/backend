import 'firebase/auth';
import 'firebase/database';
import { Observable } from 'rxjs';
import { Walker, EventLike, ResourceQuery, ThunkLike } from './backend-interfaces';
import { Action } from './actions/action';
import { ResourceHandler } from './resource-handlers/resource-handler';
export declare const resourceHandlers: {
    [key: string]: ResourceHandler;
};
export declare class Backend implements Walker {
    redux: any;
    private sbj;
    snapshoters: {
        [key: string]: ((event: Action, w: Walker) => any);
    };
    streamSnapshoter?: ((event: Action, w: Walker) => any);
    streamOnlyExternal: boolean;
    constructor();
    get<T>(q: ResourceQuery<T> | string): T;
    getAsync<T>(q: ResourceQuery<T> | string): Promise<T>;
    newKey(q: ResourceQuery<string>): Promise<string | null>;
    dispatch<T>(action: EventLike | ThunkLike<T>): Promise<T>;
    state(): Observable<Walker>;
}
