var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "redux", "redux-devtools-extension", "redux-immutable-state-invariant", "firebase/auth", "firebase/database", "rxjs", "./app", "./resource-handlers/firebase-rtdb", "./resource-handlers/local", "./store-reducer", "./actions/user-signed-in"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _ = require("lodash");
    var redux = require("redux");
    var redux_devtools_extension_1 = require("redux-devtools-extension");
    var redux_immutable_state_invariant_1 = require("redux-immutable-state-invariant");
    require("firebase/auth");
    require("firebase/database");
    var rxjs_1 = require("rxjs");
    var app_1 = require("./app");
    var firebase_rtdb_1 = require("./resource-handlers/firebase-rtdb");
    var local_1 = require("./resource-handlers/local");
    var store_reducer_1 = require("./store-reducer");
    var user_signed_in_1 = require("./actions/user-signed-in");
    function distinctInsert(arr, what) {
        var idx = _.indexOf(arr, what);
        if (idx !== -1) {
            return arr;
        }
        else {
            return _.concat(arr, what);
        }
    }
    exports.resourceHandlers = {
        'local': new local_1.Local(),
        'rtdb': new firebase_rtdb_1.FirebaseRTDB(),
    };
    var WalkerImpl = /** @class */ (function () {
        function WalkerImpl(backend, sub) {
            this.backend = backend;
            this.sub = sub;
            this.getters = [];
        }
        WalkerImpl.prototype.get = function (q) {
            var qry = typeof q === 'string' ? { type: 'local', path: q } : q;
            var storeKey = exports.resourceHandlers[qry.type].toString(qry);
            this.getters = distinctInsert(this.getters, storeKey);
            return this.backend.get(q);
        };
        WalkerImpl.prototype.getAsync = function (q) {
            var qry = typeof q === 'string' ? { type: 'local', path: q } : q;
            var storeKey = exports.resourceHandlers[qry.type].toString(qry);
            this.getters = distinctInsert(this.getters, storeKey);
            return this.backend.getAsync(q);
        };
        WalkerImpl.prototype.newKey = function (q) {
            return this.backend.newKey(q);
        };
        WalkerImpl.prototype.dispatch = function (event) {
            return this.backend.dispatch(event);
        };
        WalkerImpl.prototype.next = function (val) {
            if (val == null || _.includes(this.getters, val)) {
                this.getters = [];
                this.sub.next(this);
            }
        };
        return WalkerImpl;
    }());
    var Backend = /** @class */ (function () {
        function Backend() {
            var _this = this;
            this.sbj = new rxjs_1.Subject();
            this.snapshoters = {};
            this.streamOnlyExternal = true;
            this.redux = redux.createStore(store_reducer_1.default, redux_devtools_extension_1.composeWithDevTools(redux.applyMiddleware(redux_immutable_state_invariant_1.default())));
            var app = app_1.getApp();
            if (app) {
                app.auth().onAuthStateChanged(function (listener) {
                    _this.dispatch(user_signed_in_1.userSignedIn(listener ? listener.uid : undefined, listener && listener.email ? listener.email : undefined));
                });
            }
        }
        Backend.prototype.get = function (q) {
            var qry = typeof q === 'string' ? { type: 'local', path: q } : q;
            var storeKey = exports.resourceHandlers[qry.type].toString(qry);
            var val = _.get(this.redux.getState(), [storeKey]);
            if (val !== undefined) {
                return val;
            }
            exports.resourceHandlers[qry.type].ensureLoaded(qry, this);
            return null;
        };
        Backend.prototype.getAsync = function (q) {
            return __awaiter(this, void 0, void 0, function () {
                var qry, storeKey, val;
                return __generator(this, function (_a) {
                    qry = typeof q === 'string' ? { type: 'local', path: q } : q;
                    storeKey = exports.resourceHandlers[qry.type].toString(qry);
                    val = _.get(this.redux.getState(), [storeKey]);
                    if (val !== undefined) {
                        return [2 /*return*/, Promise.resolve(val)];
                    }
                    return [2 /*return*/, exports.resourceHandlers[qry.type].ensureLoaded(qry, this)];
                });
            });
        };
        Backend.prototype.newKey = function (q) {
            return exports.resourceHandlers[q.type].newKey(q);
        };
        Backend.prototype.dispatch = function (action) {
            return __awaiter(this, void 0, void 0, function () {
                var event, snapshoters, embedSnapshoterName, snapshoterNames, streamSnapshoterName, batches, idx, snapname, snapshoter, res, batchNames, result, batchResourceHandlers, idx, key, val;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!_.isFunction(action)) return [3 /*break*/, 3];
                            event = action(this);
                            if (!(event instanceof Promise)) return [3 /*break*/, 2];
                            return [4 /*yield*/, event];
                        case 1:
                            event = _a.sent();
                            _a.label = 2;
                        case 2: return [3 /*break*/, 4];
                        case 3:
                            event = action;
                            _a.label = 4;
                        case 4:
                            if (event == null) {
                                return [2 /*return*/, event];
                            }
                            snapshoters = _.merge({}, this.snapshoters);
                            embedSnapshoterName = '@';
                            snapshoters[embedSnapshoterName] = _.get(event, 'snapshoter');
                            snapshoterNames = _.keys(snapshoters);
                            streamSnapshoterName = '@@';
                            if (this.streamSnapshoter) {
                                // stream snapshoter needs to be at the end
                                snapshoterNames.push(streamSnapshoterName); // added to the end
                                snapshoters[streamSnapshoterName] = this.streamSnapshoter; // made availble in the object
                            }
                            batches = {};
                            idx = 0;
                            _a.label = 5;
                        case 5:
                            if (!(idx < snapshoterNames.length)) return [3 /*break*/, 11];
                            snapname = snapshoterNames[idx];
                            snapshoter = snapshoters[snapname];
                            res = null;
                            if (!(snapshoter == null)) return [3 /*break*/, 6];
                            if (snapname !== embedSnapshoterName) {
                                return [3 /*break*/, 10];
                            }
                            res = _.get(event, 'snapshot');
                            return [3 /*break*/, 9];
                        case 6:
                            if (snapname === streamSnapshoterName) {
                                batchNames = _.keys(batches);
                                if (this.streamOnlyExternal && (batchNames.length === 0 || _.isEqual(batchNames, ['local']))) {
                                    return [3 /*break*/, 10];
                                }
                            }
                            res = snapshoter(event, this);
                            if (!(res instanceof Promise)) return [3 /*break*/, 8];
                            return [4 /*yield*/, res];
                        case 7:
                            res = _a.sent();
                            _a.label = 8;
                        case 8:
                            if (res != null) {
                                _.set(event, ['snapshots', snapname], res);
                            }
                            _a.label = 9;
                        case 9:
                            if (res == null) {
                                return [3 /*break*/, 10];
                            }
                            _.forIn(res, function (dataVal, dataKey) {
                                var pieces = dataKey.split(':');
                                var resultKey = dataKey;
                                if (pieces[1] !== 'local') {
                                    resultKey = _.slice(pieces, 2).join(':');
                                }
                                _.set(batches, [pieces[1], resultKey], dataVal);
                            });
                            _a.label = 10;
                        case 10:
                            ++idx;
                            return [3 /*break*/, 5];
                        case 11:
                            // add cache to batches
                            _.forIn(event['cache'], function (dataVal, dataKey) {
                                _.set(batches, ['local', dataKey], dataVal);
                            });
                            result = this.redux.dispatch(event);
                            batchResourceHandlers = _.keys(batches);
                            idx = 0;
                            _a.label = 12;
                        case 12:
                            if (!(idx < batchResourceHandlers.length)) return [3 /*break*/, 16];
                            key = batchResourceHandlers[idx];
                            val = batches[key];
                            if (!(key === 'local')) return [3 /*break*/, 13];
                            // notify all state observables
                            _.forIn(val, function (lval, lkey) {
                                var lkeyParts = lkey.split('.');
                                _this.sbj.next(lkeyParts[0]);
                            });
                            return [3 /*break*/, 15];
                        case 13: return [4 /*yield*/, exports.resourceHandlers[key].set(val)];
                        case 14:
                            _a.sent();
                            _a.label = 15;
                        case 15:
                            ++idx;
                            return [3 /*break*/, 12];
                        case 16: 
                        // return what dispatch returned
                        return [2 /*return*/, result];
                    }
                });
            });
        };
        Backend.prototype.state = function () {
            var _this = this;
            return new rxjs_1.Observable(function (sub) {
                var dbWrap = new WalkerImpl(_this, sub);
                dbWrap.next(null);
                var subs = _this.sbj.subscribe(function (val) {
                    dbWrap.next(val);
                });
                return function () {
                    subs.unsubscribe();
                    sub.complete();
                };
            });
        };
        return Backend;
    }());
    exports.Backend = Backend;
});
//# sourceMappingURL=backend.js.map