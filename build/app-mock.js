(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "firebase-nightlight"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var firebaseNighlight = require("firebase-nightlight");
    function setAppMock(stub) {
        var mock = new firebaseNighlight.Mock(stub);
        return mock.initializeApp({});
    }
    exports.default = setAppMock;
});
//# sourceMappingURL=app-mock.js.map